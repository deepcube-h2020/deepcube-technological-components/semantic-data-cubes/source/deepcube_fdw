import subprocess
from setuptools import setup, find_packages, Extension

setup(
  name='myfdw-uc3',
  version='0.0.1',
  author='Pakicious',
  license='Postgresql',
  packages=['myfdw-uc3'],
  install_requires=['xarray', 'netCDF4', 'fsspec']
)
