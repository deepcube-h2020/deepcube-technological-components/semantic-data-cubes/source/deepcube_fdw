FROM postgres:12

RUN apt-get update  &&\
     apt-get install -y build-essential python python3-dev python3-pip &&\
     apt-get autoclean  &&\
     pip3 install pgxnclient    &&\
     apt-get install -y postgresql-server-dev-12    &&\
     pgxn install multicorn &&\
     pip3 install xarray    &&\
     pip3 install zarr  &&\
     pip3 install fsspec    &&\
     apt-get -y install nano    &&\
     apt-get -y install lsb-release &&\
     apt-get -y install wget    &&\
     apt-get -y install gnupg2  &&\
     sh -c 'wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | apt-key add -'   &&\
     sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt/ `lsb_release -cs`-pgdg main" | tee /etc/apt/sources.list.d/pgdg.list'    &&\
     apt-get update &&\
     apt-get -y install postgis postgresql-12-postgis-3 &&\
     apt-get autoclean
 
EXPOSE 5432